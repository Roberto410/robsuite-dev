#include "RobSuite.hpp"
#include "../include/dsp/digital.hpp"


#define NUM_CHANNELS 10


struct ReplicatorR1 : Module {
	enum ParamIds {
		NUM_PARAMS
	};
	enum InputIds {
		IN_INPUT,
		NUM_INPUTS
	};
	enum OutputIds {
		OUT_OUTPUT,
		NUM_OUTPUTS = OUT_OUTPUT + NUM_CHANNELS
	};
	enum LightIds {
		NUM_LIGHTS
	};


	ReplicatorR1() : Module(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS) {
		onReset();
	}
	void step() override;

};

void ReplicatorR1::step() {
	if (inputs[IN_INPUT].active)
		for (int i = 0; i < NUM_CHANNELS; i++) {
			if (outputs[OUT_OUTPUT + i].active)
				outputs[OUT_OUTPUT + i].value = inputs[IN_INPUT].value;
		}
	
}


struct ReplicatorR1Widget : ModuleWidget {
	ReplicatorR1Widget(ReplicatorR1 *module);
};

ReplicatorR1Widget::ReplicatorR1Widget(ReplicatorR1 *module) : ModuleWidget(module) {
	setPanel(SVG::load(assetPlugin(plugin, "res/Replicator-R1.svg")));

	addInput(Port::create<PJ301MPort>(mm2px(Vec(6, 10)), Port::INPUT, module, ReplicatorR1::IN_INPUT));

	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 24)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 0));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 34)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 1));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 44)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 2));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 54)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 3));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 64)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 4));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 74)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 5));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 84)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 6));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 94)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 7));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 104)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 8));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 114)), Port::OUTPUT, module, ReplicatorR1::OUT_OUTPUT + 9));
}


Model *modelReplicatorR1 = Model::create<ReplicatorR1, ReplicatorR1Widget>("RobSuite", "Replicator-R1", "Replicator-R1", SWITCH_TAG);
