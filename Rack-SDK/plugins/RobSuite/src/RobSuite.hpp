#include "rack.hpp"


using namespace rack;

// Forward-declare the Plugin, defined in Template.cpp
extern Plugin *plugin;

// Forward-declare each Model, defined in each module source file
extern Model *modelVCO;
extern Model *modelMutesR1;
extern Model *modelEmptyWig;
extern Model *modelVCAR1;
extern Model *modelLeadR1;
extern Model *modelADSRR1;
extern Model *modelReplicatorR1;
extern Model *modelVCFR1;
extern Model *modelUnityR1;
extern Model *modelUnityR1;
extern Model *modelOscToolR1;
