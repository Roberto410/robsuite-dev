#include "RobSuite.hpp"


Plugin *plugin;


void init(Plugin *p) {
	plugin = p;
	p->slug = TOSTRING(SLUG);
	p->version = TOSTRING(VERSION);

	// Add all Models defined throughout the plugin
	p->addModel(modelVCO);
	p->addModel(modelMutesR1);
	p->addModel(modelEmptyWig);
	p->addModel(modelVCAR1);
	p->addModel(modelLeadR1);
	p->addModel(modelADSRR1);
	p->addModel(modelReplicatorR1);
	p->addModel(modelVCFR1);
	p->addModel(modelUnityR1);
	p->addModel(modelOscToolR1);

	// Any other plugin initialization may go here.
	// As an alternative, consider lazy-loading assets and lookup tables when your module is created to reduce startup times of Rack.
}
