#include "RobSuite.hpp"



struct EmptyWig : Module {

	enum ParamIds {
		NUM_PARAMS
	};
	enum InputIds {
		NUM_INPUTS
	};
	enum OutputIds {
		NUM_OUTPUTS
	};
	enum LightIds {
		NUM_LIGHTS
	};
	
	EmptyWig() : Module(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS) {}
	void step() override;
};


void EmptyWig::step() {
	
}


struct EmptyWigWidget : ModuleWidget {
	EmptyWigWidget(EmptyWig *module);
};

EmptyWigWidget::EmptyWigWidget(EmptyWig *module) : ModuleWidget(module) {
	setPanel(SVG::load(assetPlugin(plugin, "res/EmptyWig.svg")));

}


Model *modelEmptyWig = Model::create<EmptyWig, EmptyWigWidget>("RobSuite", "EmptyWig", "EmptyWig", OSCILLATOR_TAG);

