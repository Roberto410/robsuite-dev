#include "RobSuite.hpp"



struct VCAR1 : Module {
	enum ParamIds {
		LEVEL_PARAM,
		EXP_PARAM,
		NUM_PARAMS
	};
	enum InputIds {
		CV_INPUT,
		IN_INPUT,
		NUM_INPUTS
	};
	enum OutputIds {
		OUT_OUTPUT,
		NUM_OUTPUTS
	};
	enum LightIds {
		NUM_LIGHTS
	};

	float lastCv = 0.f;

	VCAR1() : Module(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS) {}

	void step() override {
		float cv = 1.f;
		if (inputs[CV_INPUT].active)
			cv = fmaxf(inputs[CV_INPUT].value / 10.f, 0.f);
		if ((int) params[EXP_PARAM].value == 0)
			cv = powf(cv, 4.f);
		lastCv = cv;
		//outputs[OUT_OUTPUT].value = inputs[IN_INPUT].value * params[LEVEL_PARAM].value * cv;
		outputs[OUT_OUTPUT].value = inputs[IN_INPUT].value * 1.0f * cv;
	}
};


struct VCAR1VUKnob : Knob {
	//VCAR1VUKnob() {
		//box.size = mm2px(Vec(10, 46));
	//}

	/*
	void draw(NVGcontext *vg) override {
		nvgBeginPath(vg);
		nvgRoundedRect(vg, 0, 0, box.size.x, box.size.y, 2.0);
		nvgFillColor(vg, nvgRGB(0, 0, 0));
		nvgFill(vg);

		VCAR1 *module = dynamic_cast<VCAR1*>(this->module);

		const int segs = 25;
		const Vec margin = Vec(4, 4);
		Rect r = box.zeroPos();

		for (int i = 0; i < segs; i++) {
			float segValue = clamp(value * segs - (segs - i - 1), 0.f, 1.f);
			float amplitude = value * module->lastCv;
			float segAmplitude = clamp(amplitude * segs - (segs - i - 1), 0.f, 1.f);
			nvgBeginPath(vg);
			nvgRect(vg, r.pos.x, r.pos.y + r.size.y / segs * i + 0.5,
				r.size.x, r.size.y / segs - 1.0);
			if (segValue > 0.f) {
				//nvgFillColor(vg, colorAlpha(nvgRGBf(0.33, 0.33, 0.33), segValue));
				nvgFill(vg);
			}
			if (segAmplitude > 0.f) {
				//nvgFillColor(vg, colorAlpha(COLOR_GREEN, segAmplitude));
				nvgFill(vg);
			}
		}
	}
	*/
};


struct VCAR1Widget : ModuleWidget {
	VCAR1Widget(VCAR1 *module) : ModuleWidget(module) {
		setPanel(SVG::load(assetPlugin(plugin, "res/VCA-R1.svg")));

		
		//addParam(ParamWidget::create<VCAR1VUKnob>(mm2px(Vec(2.62103, 12.31692)), module, VCAR1::LEVEL_PARAM, 0.0, 1.0, 1.0));
		addParam(ParamWidget::create<CKSS>(mm2px(Vec(5.24619, 79.9593)), module, VCAR1::EXP_PARAM, 0.0, 1.0, 1.0));

		addInput(Port::create<PJ301MPort>(mm2px(Vec(3.51261, 60.4008)), Port::INPUT, module, VCAR1::CV_INPUT));
		addInput(Port::create<PJ301MPort>(mm2px(Vec(3.51398, 97.74977)), Port::INPUT, module, VCAR1::IN_INPUT));

		addOutput(Port::create<PJ301MPort>(mm2px(Vec(3.51398, 108.64454)), Port::OUTPUT, module, VCAR1::OUT_OUTPUT));
	}
};


Model *modelVCAR1 = Model::create<VCAR1, VCAR1Widget>("RobSuite", "VCA-R1", "VCA-R1", AMPLIFIER_TAG);

