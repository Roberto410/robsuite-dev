#include "RobSuite.hpp"
#include "dsp/vumeter.hpp"

#define NUM_CHANNELS 10

struct OscToolR1 : Module {
	enum ParamIds {
		AVG1_PARAM,
		VCAEXP_PARAM,
		VCALEVEL_PARAM,
		PAN_PARAM,
		OSCLEV_PARAM = PAN_PARAM + NUM_CHANNELS,
		NUM_PARAMS = OSCLEV_PARAM + NUM_CHANNELS
	};
	enum InputIds {
		CV_INPUT,
		VCA_INPUT,
		IN1_INPUT,
		NUM_INPUTS = IN1_INPUT + NUM_CHANNELS
	};
	enum OutputIds {
		VCAL_OUTPUT,
		VCAR_OUTPUT,
		VCA_OUTPUT,
		CV_OUTPUT,
		NUM_OUTPUTS = CV_OUTPUT + NUM_CHANNELS
	};
	enum LightIds {
		VU1_LIGHT,
		VU2_LIGHT = VU1_LIGHT + 5,
		NUM_LIGHTS = VU2_LIGHT + 5
	};

	bool merge = false;


	OscToolR1() : Module(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS) {}
	void step() override;

	void onReset() override {
		merge = false;
	}

	json_t *toJson() override {
		json_t *rootJ = json_object();
		// merge
		json_object_set_new(rootJ, "merge", json_boolean(merge));
		return rootJ;
	}

	void fromJson(json_t *rootJ) override {
		// merge
		json_t *mergeJ = json_object_get(rootJ, "merge");
		if (mergeJ)
			merge = json_boolean_value(mergeJ);
	}
};

void OscToolR1::step() {
	float mix = {};
	float mixL = {};
	float mixR = {};
	int count = {};


	if (inputs[CV_INPUT].active)
	{
		for (int i = 0; i < NUM_CHANNELS; i++) {
			if (outputs[CV_OUTPUT + i].active)
				outputs[CV_OUTPUT + i].value = inputs[CV_INPUT].value;
		}
	}
	
		// Inputs
	for (int j = 0; j < NUM_CHANNELS; j++) {


		float channels = inputs[IN1_INPUT + j].value * params[OSCLEV_PARAM + j].value;
		if (outputs[VCA_OUTPUT].active)
			mix += channels;
		if (outputs[VCAR_OUTPUT].active)
			mixR += channels * params[PAN_PARAM + j].value;
		if (outputs[VCAL_OUTPUT].active)
			mixL += channels * (1.0 - params[PAN_PARAM + j].value);
		if (inputs[IN1_INPUT + j].active)
			count++;
	}

	// Params
	if ((int) params[AVG1_PARAM].value == 1 && count > 0)
		mix /= count;



	float vca_cv = 1.f;
	if (inputs[VCA_INPUT].active)
		vca_cv = fmaxf(inputs[VCA_INPUT].value / 10.f, 0.f);
	if ((int)params[VCAEXP_PARAM].value == 0)
		vca_cv = powf(vca_cv, 4.f);
	float level = params[VCALEVEL_PARAM].value * vca_cv;




	if (outputs[VCA_OUTPUT].active)
		outputs[VCA_OUTPUT].value = mix * level;
	if (outputs[VCAL_OUTPUT].active)
		outputs[VCAL_OUTPUT].value = mixL * level;
	if (outputs[VCAR_OUTPUT].active)
		outputs[VCAR_OUTPUT].value = mixR * level;

}



struct OscToolWidgetR1 : ModuleWidget {
	OscToolWidgetR1(OscToolR1 *module);
	void appendContextMenu(Menu *menu) override;
};

OscToolWidgetR1::OscToolWidgetR1(OscToolR1 *module) : ModuleWidget(module) {
	setPanel(SVG::load(assetPlugin(plugin, "res/OscTool-R1.svg")));


	addParam(ParamWidget::create<CKSS>(mm2px(Vec(62, 70)), module, OscToolR1::AVG1_PARAM, 0.0f, 1.0f, 0.0f));

	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 24)), Port::INPUT, module, OscToolR1::IN1_INPUT + 0));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 34)), Port::INPUT, module, OscToolR1::IN1_INPUT + 1));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 44)), Port::INPUT, module, OscToolR1::IN1_INPUT + 2));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 54)), Port::INPUT, module, OscToolR1::IN1_INPUT + 3));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 64)), Port::INPUT, module, OscToolR1::IN1_INPUT + 4));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 74)), Port::INPUT, module, OscToolR1::IN1_INPUT + 5));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 84)), Port::INPUT, module, OscToolR1::IN1_INPUT + 6));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 94)), Port::INPUT, module, OscToolR1::IN1_INPUT + 7));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 104)), Port::INPUT, module, OscToolR1::IN1_INPUT + 8));
	addInput(Port::create<PJ301MPort>(mm2px(Vec(18, 114)), Port::INPUT, module, OscToolR1::IN1_INPUT + 9));


	//CV Replication
	addInput(Port::create<PJ301MPort>(mm2px(Vec(60, 20)), Port::INPUT, module, OscToolR1::CV_INPUT));

	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 24)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 0));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 34)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 1));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 44)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 2));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 54)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 3));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 64)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 4));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 74)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 5));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 84)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 6));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 94)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 7));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 104)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 8));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(6, 114)), Port::OUTPUT, module, OscToolR1::CV_OUTPUT + 9));


	//VCA Module
	addInput(Port::create<PJ301MPort>(mm2px(Vec(60, 30)), Port::INPUT, module, OscToolR1::VCA_INPUT));

	addOutput(Port::create<PJ301MPort>(mm2px(Vec(60, 84)), Port::OUTPUT, module, OscToolR1::VCA_OUTPUT));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(60, 94)), Port::OUTPUT, module, OscToolR1::VCAL_OUTPUT));
	addOutput(Port::create<PJ301MPort>(mm2px(Vec(60, 104)), Port::OUTPUT, module, OscToolR1::VCAR_OUTPUT));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(60, 114)), module, OscToolR1::VCALEVEL_PARAM, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<CKSS>(mm2px(Vec(68, 70)), module, OscToolR1::VCAEXP_PARAM, 0.0, 1.0, 1.0));

	//Levels
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 24)), module, OscToolR1::OSCLEV_PARAM + 0, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 34)), module, OscToolR1::OSCLEV_PARAM + 1, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 44)), module, OscToolR1::OSCLEV_PARAM + 2, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 54)), module, OscToolR1::OSCLEV_PARAM + 3, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 64)), module, OscToolR1::OSCLEV_PARAM + 4, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 74)), module, OscToolR1::OSCLEV_PARAM + 5, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 84)), module, OscToolR1::OSCLEV_PARAM + 6, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 94)), module, OscToolR1::OSCLEV_PARAM + 7, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 104)), module, OscToolR1::OSCLEV_PARAM + 8, 0.0, 1.0, 1.0));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(30, 114)), module, OscToolR1::OSCLEV_PARAM + 9, 0.0, 1.0, 1.0));



	//Pan
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 24)), module, OscToolR1::PAN_PARAM + 0, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 34)), module, OscToolR1::PAN_PARAM + 1, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 44)), module, OscToolR1::PAN_PARAM + 2, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 54)), module, OscToolR1::PAN_PARAM + 3, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 64)), module, OscToolR1::PAN_PARAM + 4, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 74)), module, OscToolR1::PAN_PARAM + 5, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 84)), module, OscToolR1::PAN_PARAM + 6, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 94)), module, OscToolR1::PAN_PARAM + 7, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 104)), module, OscToolR1::PAN_PARAM + 8, 0.0, 1.0, 0.5));
	addParam(ParamWidget::create<RoundSmallBlackKnob>(mm2px(Vec(42, 114)), module, OscToolR1::PAN_PARAM + 9, 0.0, 1.0, 0.5));
}


struct OscToolR1MergeItem : MenuItem {
	OscToolR1 *OscToolR1;
	void onAction(EventAction &e) override {
		OscToolR1->merge ^= true;
	}
	void step() override {
		rightText = CHECKMARK(OscToolR1->merge);
	}
};

void OscToolWidgetR1::appendContextMenu(Menu *menu) {
	menu->addChild(MenuEntry::create());

	OscToolR1 *unityR1 = dynamic_cast<OscToolR1*>(module);
	assert(unityR1);

	OscToolR1MergeItem *mergeItem = MenuItem::create<OscToolR1MergeItem>("Merge channels 1 & 2");
	mergeItem->OscToolR1 = unityR1;
	menu->addChild(mergeItem);
}



Model *modelOscToolR1 = Model::create<OscToolR1, OscToolWidgetR1>("RobSuite", "OscTool-R1", "OscTool-R1", MIXER_TAG, UTILITY_TAG, DUAL_TAG);
